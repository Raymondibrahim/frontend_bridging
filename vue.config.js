module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/bridging/'
      : '/',
    devServer: {
      socket: 'socket'
    },
  }
  