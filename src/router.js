import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
function importComponent(path) {
 return () => import(`./components/${path}.vue`)
}
Vue.use(VueRouter);
const router = new VueRouter(
    {
    mode: "history",
    routes: [
        {
        path: "/",
        redirect: { name: "Login" },
        component: importComponent('DashboardLayout'),
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem("token") != null) {
              next();
            } else {
              next({ name: "Login" });
            }
          },
        children: [
                //Mahasiswa Siatma
                {
                    path: "/mhs_siatma",
                    name: "Mahasiswa Siatma",
                    meta: {title: 'Mahasiswa Siatma'},
                    component: importComponent('SIATMA/MahasiswaSiatma'),
                },
                //Prodi Siatma
                {
                    path: "/prodi_siatma",
                    name: "Program Studi Siatma",
                    meta: {title: 'Program Studi Siatma'},
                    component: importComponent('SIATMA/ProdiSiatma'),
                },
                //Fakultas Siatma
                {
                    path: "/fakultas_siatma",
                    name: "Fakultas Siatma",
                    meta: {title: 'Fakultas Siatma'},
                    component: importComponent('SIATMA/FakultasSiatma'),
                },
                //Fakultas Siatma
                {
                    path: "/matkul_siatma",
                    name: "Matkul Siatma",
                    meta: {title: 'Matkul Siatma'},
                    component: importComponent('SIATMA/MatkulSiatma'),
                },
                //Semester Akademik Siatma
                {
                    path: "/smaksi",
                    name: "Semester Akademik Siatma",
                    meta: {title: 'Semester Akademik Siatma'},
                    component: importComponent('SIATMA/SmstAkademikSiatma'),
                },
                //Dosen Siatma
                {
                    path: "/dosen_siatma",
                    name: "Dosen Siatma",
                    meta: {title: 'Dosen Siatma'},
                    component: importComponent('SIATMA/DosenSiatma'),
                },
                //KRS Siatma
                {
                    path: "/krs_siatma",
                    name: "KRS Siatma",
                    meta: {title: 'KRS Siatma'},
                    component: importComponent('SIATMA/KrsSiatma'),
                },
                //Mahasiswa Baru
                {
                    path: "/mhs_baru",
                    name: "Mahasiswa Baru",
                    meta: {title: 'Mahasiswa Baru'},
                    component: importComponent('BARU/MahasiswaBaru'),
                },
                //Dosen Baru
                {
                    path: "/dosen_baru",
                    name: "Dosen Baru",
                    meta: {title: 'Dosen Baru'},
                    component: importComponent('BARU/DosenBaru'),
                },
                //Prodi Baru
                {
                    path: "/prodi_baru",
                    name: "Prodi Baru",
                    meta: {title: 'Prodi Baru'},
                    component: importComponent('BARU/ProdiBaru'),
                },
                //Fakultas Baru
                {
                    path: "/fakultas_baru",
                    name: "Fakultas Baru",
                    meta: {title: 'Fakultas Baru'},
                    component: importComponent('BARU/FakultasBaru'),
                },
                //Matkul Baru
                {
                    path: "/matkul_baru",
                    name: "Matkul Baru",
                    meta: {title: 'Matkul Baru'},
                    component: importComponent('BARU/MatkulBaru'),
                },
                //Semester Akademik Baru
                {
                    path: "/smak_baru",
                    name: "Semester Akademik Baru",
                    meta: {title: 'Semester Akademik Baru'},
                    component: importComponent('BARU/SmakBaru'),
                },
                //KRS Baru
                {
                    path: "/krs_baru",
                    name: "Krs Baru",
                    meta: {title: 'Krs Baru'},
                    component: importComponent('BARU/KrsBaru'),
                },
                //KRS Moodle
                {
                    path: "/krs_moodle",
                    name: "Krs Moodle",
                    meta: {title: 'Krs Moodle'},
                    component: importComponent('MOODLE/KrsMoodle'),
                },
                //Mahasiswa Moodle
                {
                    path: "/mhs_moodle",
                    name: "Mahasiswa Moodle",
                    meta: {title: 'Mahasiswa Moodle'},
                    component: importComponent('MOODLE/MahasiswaMoodle'),
                },
                //Dosen Moodle
                {
                    path: "/dsnmoodle",
                    name: "Dosen Moodle",
                    meta: {title: 'Dosen Moodle'},
                    component: importComponent('MOODLE/DosenMoodle'),
                },
                //KRS Dosen Baru
                {
                    path: "/krsdsn_baru",
                    name: "KRS Dosen Baru",
                    meta: {title: 'KRS Dosen Baru'},
                    component: importComponent('BARU/KrsDosenBaru'),
                },
                //KRS Dosen Baru
                {
                    path: "/krsdsn_moodle",
                    name: "Krs Dosen Moodle",
                    meta: {title: 'Krs Dosen Moodle'},
                    component: importComponent('MOODLE/KrsDosenMoodle'),
                },
                //Matkul Moodle
                {
                    path: "/matkul_moodle",
                    name: "Mata Kuliah Moodle",
                    meta: {title: 'Mata Kuliah Moodle'},
                    component: importComponent('MOODLE/MatkulMoodle'),
                },
                //User
                {
                    path: "/user",
                    name: "User Bridging",
                    meta: {title: 'User Bridging'},
                    component: importComponent('User'),
                },
            ]
        },
        //Login
        {
            path: "/Login",
            name: "Login",
            meta: {title: 'Login'},
            component: importComponent('Login'),
        },
        //Logout
        {
            path: "/logout",
            name: "logout",
            meta: {title: 'Logout'},
        },
        //Dashboard
        {
            path: "/dashboard",
            name: "Dashboard",
            meta: {title: 'Dashboard'},
            component: importComponent('Dashboard'),
        },
        {
            path: '*',
            redirect: '/'
        },
    ],
});

//mengset judul
router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    next()
});

export default router;